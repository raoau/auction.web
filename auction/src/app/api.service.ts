import { Injectable } from '@angular/core';
import { Auction } from './model/auction';
import { AuctionProduct } from './model/auctionproduct';
import { Guid } from "guid-typescript";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class ApiService {

  auction: Auction;
  auctionProducts: AuctionProduct[];
  api_url = "http://localhost:42000/api/bid";
  api_bid_path = "";
  api_auctionproducts_path = "/auctionproducts";

  constructor(private httpClient: HttpClient) { }

  getLatestAuctionProducts(): Observable<any> {
    return this.httpClient.get(this.api_url + this.api_auctionproducts_path);
  }

  postBids(body: any): Observable<any> {
    const headers = new HttpHeaders(
      {
          'Content-Type': 'application/json'
      });
    return this.httpClient.post(this.api_url + this.api_bid_path, body, {headers: headers});
  }
}
