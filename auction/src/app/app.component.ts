import { Component } from "@angular/core";
import { ApiService } from "./api.service";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  FormArray
} from "@angular/forms";
import { Guid } from "guid-typescript";
import { analyzeAndValidateNgModules } from "@angular/compiler";
import { AuctionProduct } from "./model/auctionproduct";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "auction";
  auctionProducts: AuctionProduct[];
  userId = "11111111-1111-1111-1111-111111111111";

  auctionForm: FormGroup;
  bids: FormArray;

  clicked = false;

  result = "";

  constructor(
    private formBuilder: FormBuilder,
    private apiService: ApiService
  ) {}

  ngOnInit() {
    this.getLatestAuctionProducts();
  }

  addItems() {
    this.auctionProducts.forEach(ap => {
      this.bids = this.auctionForm.get("bids") as FormArray;
      this.bids.push(
        this.formBuilder.group({
          Id: ap.Id,
          UserId: this.userId,
          ProductName: ap.ProductName,
          InitialPrice: ap.InitialPrice,
          BidPrice: [""]
        })
      );
    });
  }

  getLatestAuctionProducts() {
    this.apiService.getLatestAuctionProducts().subscribe((res: any[]) => {
      console.log(res);
      this.auctionProducts = res;

      this.auctionForm = this.formBuilder.group({
        bids: this.formBuilder.array([])
      });

      this.addItems();
    });
  }

  onSubmit() {

    this.clicked = true;

    let formObj = this.auctionForm.getRawValue();
    let serializedForm = JSON.stringify(formObj);

    console.log(formObj);
    console.log(serializedForm);

    this.apiService.postBids(serializedForm).subscribe((res: any[]) => {
      console.log(res);
    },
    error => {
        console.log(error);
        this.result = error;
    },
    () => {

        console.log("OK");
        this.result = "Done";
    });
  }
}
