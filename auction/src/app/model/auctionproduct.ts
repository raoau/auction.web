import { Guid } from "guid-typescript";

export class AuctionProduct {
  Id: string;
  AuctionId: string;
  ProductId: string;
  ProductName: string;
  InitialPrice: number;
  SoldPrice: number;
}
