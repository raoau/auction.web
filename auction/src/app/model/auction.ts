import { Guid } from "guid-typescript";

export class Auction {
    Id: Guid;
    Name: string;
    BeginTime: Date;
    EndTime: Date;
}