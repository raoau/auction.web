import { Guid } from "guid-typescript";

export class Bid {
    AuctionProductId: string;
    UserId: string;
    BidPrice: number;
}